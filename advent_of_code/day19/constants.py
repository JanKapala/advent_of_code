import os

from definitions import PROJECT_ROOT_PATH

DAY19 = "day19"
DAY_19_ROOT_DIR = os.path.join(PROJECT_ROOT_PATH, "advent_of_code", DAY19)
DAY_19_LOG_DIR = os.path.join(PROJECT_ROOT_PATH, "runs", DAY19)
DAY_19_INPUT_FILE_PATH = os.path.join(DAY_19_ROOT_DIR, "input.txt")
