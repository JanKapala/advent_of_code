import os

from invoke import task

from definitions import PROJECT_ROOT_PATH

SOURCES_PATH = os.path.join(PROJECT_ROOT_PATH, "advent_of_code")
TESTS_PATH = os.path.join(PROJECT_ROOT_PATH, "tests")

@task
def test(c, type):
    if type == "coverage":
        c.run(
            f"poetry run pytest -n auto --randomly-seed=1234 --cov={SOURCES_PATH} {TESTS_PATH}"
        )
    else:
        specific_tests_path = os.path.join(TESTS_PATH, type)
        c.run(f"poetry run pytest -n auto --randomly-seed=1234 {specific_tests_path}")


@task
def mypy(c):
    c.run(f"poetry run mypy --install-types --non-interactive {SOURCES_PATH} {TESTS_PATH} definitions.py tasks.py")


@task
def black(c, only_check: bool = False):
    if only_check:
        command = f"poetry run black --check {SOURCES_PATH} {TESTS_PATH} definitions.py tasks.py"
    else:
        command = f"poetry run black {SOURCES_PATH} {TESTS_PATH} definitions.py tasks.py"
    c.run(command)


@task
def isort(c, only_check: bool = False):
    if only_check:
        command = f"poetry run isort --check {SOURCES_PATH} {TESTS_PATH} definitions.py tasks.py"
    else:
        command = f"poetry run isort {SOURCES_PATH} {TESTS_PATH} definitions.py tasks.py"
    c.run(command)


@task
def lint(c):
    c.run(f"poetry run pylint --jobs=0 --recursive=y {SOURCES_PATH} {TESTS_PATH} definitions.py tasks.py")


@task
def bandit(c):
    c.run(f"poetry run bandit -c pyproject.toml -r {SOURCES_PATH}")








