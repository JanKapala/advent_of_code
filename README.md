[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Build](https://github.com/JanKapala/advent_of_code/actions/workflows/python-app.yml/badge.svg)](https://github.com/JanKapala/advent_of_code/actions/workflows/python-app.yml)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)

# Advent of Code 2022 RL
RL solutions to the Advent of Code 2022 problems.

### Project setup

sudo apt install -y python3.11-dev
sudo apt install -y python3.11-tk
sudo apt install -y liblzma-dev


- Install [pyenv](https://github.com/pyenv/pyenv)
- Install Python 3.11.2 (via pyenv):
  - You may need to run `sudo apt install -y python3.11-dev python3.11-tk liblzma-dev libsqlite3-dev` before
- Set python version: `pyenv local 3.11.2`
- Install Package manager: todo poetry

- Install project dependencies: `poetry install`
- [pre-commit](https://pre-commit.com/) `pre-commit install`

- Install [PyCharm](https://www.jetbrains.com/pycharm/)
- This project uses Google style docstrings, set it in the Pycharm settings | Tools | Python Integrated Tools and also check following checkboxes:
  - Analyze Python code in docstrings
  - Render external documentation for stdlib
- Mark <PROJECT_ROOT>/.venv/lib/python3.11 as a `Sources Root` in the PyCharm

- Install [Graphviz]() with:
  - `sudo apt install graphviz`


Install and register gitlab runner

Set "Run untagged jobs" in the gitlab CI -> runners -> previously setup runner
Set concurrent = 30 in the /etc/gitlab-runner/config.toml
